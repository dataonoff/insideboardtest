const fs = require('fs');
const input = process.argv.slice(2);
if (input.length < 1) process.exit(1);

const items = JSON.parse(fs.readFileSync(input[0])).items;
let output = '';
let totalTax = 0;
let orderTotal = 0;

const taxCalculator =  (pht, t) => {
  return Number((Math.ceil((pht * (t/100)) * 20)/20).toFixed(2));
};

if (items) {
  items.forEach((item, i) => {
    let taxTotalPerItem = 0;
    //check if item is imported
    if (item.isImported) {
      taxTotalPerItem += taxCalculator(item.price, 5);
    }
    //check if item should be taxed
    if (item.category === 'global') {
      taxTotalPerItem += taxCalculator(item.price, 10);
    }
    const ttcPrice = Number((item.price + taxTotalPerItem).toFixed(2));
    output += `${item.quantity} ${item.label} : ${ttcPrice}\n`;
    totalTax += taxTotalPerItem;
    orderTotal += ttcPrice;
    if (i === items.length - 1) {
      output += `Montant des taxes : ${totalTax.toFixed(2)}\n`;
      output += `Total : ${orderTotal.toFixed(2)}\n`;
    }
  });
}

console.log(output);