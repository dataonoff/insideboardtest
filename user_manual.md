 User Manual: Inside Board test 
======================

#### Requirement
These two tests are compiled using `node.js`.

Dev version: v.10.15


# Tax computation

Please find the rules of this test inside the `taxCalculator` folder in the `taxComputation.md`.

Each order is given as argument through a `JSON` file path.
You will find the 3 orders in the `commandes_HT` folder.

In order to proceed to the tax computation you can use the following command line:

`npm run computeMyTaxs taxCalculator/commandes_HT/commande1.json`


# give Back Change

Please find the rules of this test inside the `giveBackChange` folder in the `giveBackChange.md`.

Each 'money change input' is given as argument .

Example:

`npm run giveMyMoneyBack 6`