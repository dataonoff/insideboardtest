const inputs = process.argv.slice(2);
if (inputs.length < 1) process.exit(1);

let input = BigInt(inputs[0]);
let reste = 0;
let output = '';

let monnaie = [
  { valeur: 10, nombre: 0 },
  { valeur: 5, nombre: 0 },
  { valeur: 2, nombre: 0 }
];

for (let i = 0; i < monnaie.length; i++) {
  reste = input % BigInt(monnaie[i].valeur);
  if (reste == 1 && monnaie[i + 1]) continue;
  monnaie[i].nombre = (input - reste) / BigInt(monnaie[i].valeur);
  input = reste;
}

if (
  monnaie.reduce((a, b) => ({ nombre: BigInt(a.nombre) + BigInt(b.nombre) }))
    .nombre != 0
) {
  for (let i = monnaie.length - 1; i >= 0; i--) {
    output += `${monnaie[i].valeur}x${monnaie[i].nombre}`;
    if (i === 0) break;
    output += " + ";
  }
}

console.log(output);
